# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-botocore
# Verify required version from py3-boto3 on this package before upgrading
pkgver=1.34.104
pkgrel=0
pkgdesc="The low-level, core functionality of Boto3"
url="https://github.com/boto/botocore"
arch="noarch"
license="Apache-2.0"
depends="
	py3-certifi
	py3-dateutil
	py3-jmespath
	py3-urllib3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/botocore/botocore-$pkgver.tar.gz"
builddir="$srcdir/botocore-$pkgver"
options="!check" # take way too long

replaces=py-botocore # Backwards compatibility
provides=py-botocore=$pkgver-r$pkgrel # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
8cdb6b2977bc847b39b0167b168b51c7a2c32554b709270030d64877209a9f58a60514014c2cd3251ec911f9bcc8a5231e65bc396e48a5381eabdac895173fd0  botocore-1.34.104.tar.gz
"

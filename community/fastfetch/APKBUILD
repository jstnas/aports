# Contributor: Carter Li <zhangsongcui@live.cn>
# Maintainer: Carter Li <zhangsongcui@live.cn>
pkgname=fastfetch
pkgver=2.12.0
pkgrel=0
pkgdesc="Like neofetch, but much faster because written mostly in C."
url="https://github.com/fastfetch-cli/fastfetch"
arch="all"
license="MIT"
depends="
	hwdata-pci
	"
makedepends="
	cmake samurai
	yyjson-dev
	yyjson-static
	vulkan-loader-dev
	libxcb-dev
	wayland-dev
	libdrm-dev
	dconf-dev
	imagemagick-dev
	chafa-dev
	zlib-dev
	dbus-dev
	mesa-dev
	opencl-dev
	xfconf-dev
	sqlite-dev
	networkmanager-dev
	pulseaudio-dev
	ddcutil-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/fastfetch-cli/fastfetch/archive/refs/tags/$pkgver.tar.gz"


prepare() {
	default_prepare

	rm -rf src/3rdparty/yyjson
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=Release \
		-DENABLE_SYSTEM_YYJSON=ON \
		-DENABLE_DIRECTX_HEADERS=OFF \
		$crossopts
	cmake --build build --target fastfetch --target flashfetch
}

check() {
	build/fastfetch --list-features
	build/fastfetch -c presets/ci.jsonc
	build/fastfetch -c presets/ci.jsonc --format json
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ec249afb49066eb70138503062779612c4c45dfbb44429fdfea8b5bb5f310cd5bfadde7e0f21cba5a343007ab4dee0d7e3ddf7bac84791dc0c48be57ff645d9a  fastfetch-2.12.0.tar.gz
"

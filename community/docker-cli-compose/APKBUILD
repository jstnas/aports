# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-compose
pkgver=2.27.0
pkgrel=0
pkgdesc="Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/compose/cli-command"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/docker/compose/archive/v$pkgver.tar.gz"

provides="docker-compose=$pkgver-r$pkgrel"

# secfixes:
#   2.15.1-r0:
#     - CVE-2022-27664
#     - CVE-2022-32149
#   2.12.1-r0:
#     - CVE-2022-39253

_plugin_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/compose-"$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	PKG=github.com/docker/compose/v2
	local ldflags="-X '$PKG/internal.Version=v$pkgver'"
	go build -ldflags="$ldflags" -o docker-compose ./cmd
}

check() {
	# e2e tests are excluded because they depend on live dockerd/kubernetes/ecs
	local pkgs="$(go list ./... | grep -Ev '/(watch|e2e)(/|$)')"
	go test -short -skip '^TestPs$' $pkgs
	./docker-compose version
}

package() {
	install -Dm755 docker-compose -t "$pkgdir$_plugin_installdir"/

	mkdir -p "$pkgdir"/usr/bin
	ln -sfv ../libexec/docker/cli-plugins/docker-compose "$pkgdir"/usr/bin/docker-compose
}

sha512sums="
f31627a00002416be6fb4343434d6b1e2b5d275a484ede505406bdbe65c73d61b497e849bb3645900dbb4eb3b7e638976253109b01d67776deed2a95950e3630  docker-cli-compose-2.27.0.tar.gz
"
